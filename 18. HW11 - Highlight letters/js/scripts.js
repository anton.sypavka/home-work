function highlightButton() {

    const button = document.getElementsByTagName('button');

    document.onkeydown = function (event) {

        for (const element of button) {
            element.classList.remove('active');

            if (event.key === element.dataset.letter) {
                element.classList.add('active');
            }
        }
    }
}

highlightButton();

















