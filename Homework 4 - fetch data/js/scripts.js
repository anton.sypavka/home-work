function request(url) {
    return fetch(url)
        .then(response => response.json());
};


function getFilms(url) {
    const ulWrapper = document.createElement('ul');
    document.body.append(ulWrapper);

    request(url)
        .then(({results}) => {
            results.forEach(el => {
                const {title, episode_id, opening_crawl, characters} = el;
                const filmList = document.createElement('li');
                filmList.innerHTML = `<strong>Film:</strong> ${title} <br><strong>Episode:</strong> ${episode_id} <br><strong>Opening crawl:</strong> ${opening_crawl}`;
                ulWrapper.append(filmList);

                const charUl = document.createElement('ul');
                filmList.append(charUl);

                characters.forEach(url => {
                    request(url)
                        .then(({name}) => {
                            const charLi = document.createElement('li')
                            charLi.innerHTML = name;
                            charUl.append(charLi);
                            filmList.append(charUl);
                        });
                });
            });
        });
};

getFilms('https://swapi.dev/api/films/');





