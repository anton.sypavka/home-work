/*const tabs = document.getElementsByClassName('tabs-title');

/!*
* Первый вариант с получением коллекции элементов, которым присвоен класс.
 *!/
// const tabsContent = document.getElementsByClassName('tabs-content');

/!*
* Второй вариант получения списка элементов без назначения классов в html через querySelectorAll.
* Вариант является более универсальным, так как при динамическом добавлении элементов им не нужно
* назначать класс вручную.
*!/
const tabsContent = document.querySelectorAll('ul.tabs-content > li');

let activeTab;
let activeTabContent;

for (let i = 0; i < tabs.length; i++) {

    tabs[i].onclick = function () {
        if (activeTab) {
            activeTab.classList.toggle('active');
            activeTabContent.classList.toggle('show');
        }

        activeTabContent = tabsContent[i];
        activeTab = tabs[i];
        tabs[i].classList.toggle('active');
        tabsContent[i].classList.toggle('show');
    }

}*/

function showTabs() {

    const tabs = document.querySelectorAll('.tabs-title');
    const tabsContent = document.querySelectorAll('.tab');
    let dataTab;

    for (const element of tabs) {
        element.addEventListener('click', toggleActive)
    }

    function toggleActive() {
        for (const element of tabs) {
            element.classList.remove('active');
        }
        this.classList.add('active');
        dataTab = this.getAttribute('data-tab');
        checkedDataTab(dataTab);
    }

    function checkedDataTab(dataTab) {
        for (const element of tabsContent){
            element.classList[element.classList.contains(dataTab) ? 'add' : 'remove']('active');

         }
     }


    tabs.forEach(element => element.addEventListener('click', function () {
        alert(element);
    }))

}

showTabs();














