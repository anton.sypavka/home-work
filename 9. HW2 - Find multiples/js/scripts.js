/*Первая часть задания*/

let num = +prompt('Введите число');

while (!Number.isInteger(num)) {
    num = +prompt('Введите целое число');
}

if (num < 5) {
    console.log('Sorry, no numbers');
}

/* i = 1, чтобы не выводить в консоли 0 если не чисел кратных 5*/
for(let i = 1; i <= num; i++) {
    if (i % 5 === 0) {
        console.log(i);
    }
}

/*Вторая часть продвинутого задания - выводим простые числа из диапазона.

let m = prompt('Введите минимальное число');
let n = prompt('Введите максимальное число');

firstCheck:
for (let i = m; i <= n; i++) {

    for (let j = 2; j < i; j++) {
        if (i % j === 0) continue firstCheck;

    }
    console.log(i)
}*/


