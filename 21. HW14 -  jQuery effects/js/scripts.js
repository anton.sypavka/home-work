$(document).ready(function() {
    $(".nav-link").click(function() {
        $("html, body").animate({
            scrollTop: $($(this).attr("href")).offset().top
        }, 1000)
        return false;
    });


    const btn = $('#back-to-top-btn');

    $(window).scroll(function () {
        if($(window).scrollTop() > 500) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    })

    btn.on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 600);
    });

    $('.btn-slide').click(function () {
        $('.slide-panel').slideToggle('slow');
    });

});

