
function getValueFromInput(value) {
    const container = document.getElementById('container');
    const input = document.getElementById('input-field');

    function createDomElement (tag, text, classOfElement, classOfInput) {
        const element = document.createElement(tag);
        element.innerHTML = text;
        element.classList.add(classOfElement);
        input.classList.add(classOfInput);
        return element;
    }


    const currentPrice = createDomElement('span', `Текущая цена: ${value}`, 'current-price', 'input-field-active');
    const error = createDomElement ('span', 'Please enter correct price', 'enter-correct-data');
    const buttonClose = createDomElement('button', '&times', 'close-button');


    buttonClose.onclick = function () {
        currentPrice.remove();
        input.value = '';
    }


    if (value < 0) {
        container.append(error);
        input.classList.add('input-field-error');
        return;
    }

    container.prepend(currentPrice);
    currentPrice.append(buttonClose);
}
