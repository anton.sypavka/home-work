import {ServerRequest} from "./ServerRequest.js";

export class Location extends ServerRequest {
    constructor() {
        super();
        this.url = 'http://ip-api.com/json/';
    }

    async getLocationByIp (ip) {
        return await Location.getRequest(`${this.url}${ip}`);
    };
}