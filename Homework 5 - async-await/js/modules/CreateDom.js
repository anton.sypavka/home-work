export class CreateDom {
    constructor(props) {
        const {
            country,
            city,
            timezone,
            zip
        } = props;

        this.country = country;
        this.city = city;
        this.timezone = timezone;
        this.zip = zip;
    }

    createList() {
        return `<ul><li><strong>Country:</strong> ${this.country}</li><li><strong>City:</strong> ${this.city}</li><li><strong>Timezone:</strong> ${this.timezone}</li><li><strong>Zip:</strong> ${this.zip}</li></ul>`

    }
}

