export class ServerRequest {
    constructor() {
    }

    static async getRequest(url) {
        const response = await fetch(url);
        return response.json();
    };

};

