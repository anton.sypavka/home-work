import {ServerRequest} from "./modules/ServerRequest.js";
import {Location} from "./modules/Location.js";
import {CreateDom} from "./modules/CreateDom.js";

document.getElementById('button').addEventListener('click', onBtnClick);

async function onBtnClick () {
    const {ip} = await ServerRequest.getRequest('https://api.ipify.org/?format=json');
    const data = await new Location().getLocationByIp(ip);
    document.body.insertAdjacentHTML('beforeend', new CreateDom(data).createList());
};




