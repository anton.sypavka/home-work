document.addEventListener('DOMContentLoaded', onReady);


function onReady() {
    const images = document.querySelectorAll('img');
    const stopButton = document.getElementById('stop');
    const resumeButton = document.getElementById('resume');
    let i = 0;
    let timer;


    function slider () {
        i++
        if(i >= images.length){
            i = 0;
        }
        for(const element of images){
            element.classList.add('display-none');
        }
        images[i].classList.remove('display-none');
        timer = setTimeout( slider, 10000);
    }
    slider ();

    stopButton.addEventListener('click', function () {
        clearTimeout(timer);
    })

    resumeButton.addEventListener('click', slider);
}






















