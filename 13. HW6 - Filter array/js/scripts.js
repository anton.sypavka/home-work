/**
* @desc 1й вариант функции с фильтром через if и typeof
 **/
function filterBy(arr, dt = 'string') {
   const newArr = [];

    arr.forEach(element => {
        if (typeof element !== dt ) {
            newArr.push(element);
        }
        })
    return newArr;
}



const array = ['hello', 'world', 23, '23', null, 'string'];
const dataType = 'string';
const excludedArray = filterBy(array, dataType);
console.log(excludedArray);


/**
 * @desc 2й вариант функции с использованием фильтра. Функция возвращает новый массив.
 **/
// function filterBy(arr, dt = 'string') {
//     return arr.filter(element => typeof element !== dt);
//
// }
//
// const array = ['hello', 'world', 23, '23', null, 'string'];
// const dataType = 'string';
// const excludedArray = filterBy(array, dataType);
// console.log(excludedArray);
