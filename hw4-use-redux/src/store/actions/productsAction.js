import ProductService from "../../services/productService";


export const loadProducts = () => {
    return async dispatch => {
        dispatch(productsRequested());

        await ProductService.loadProducts()
            .then(result => {
                setTimeout( () => {
                    dispatch(productsSuccess(result));
                }, 1500);
            })
            .catch(error => {
                dispatch(productsError(error.message));
            });
    }
}

const productsRequested = () => {
    return {
        type: 'GET_PRODUCTS_REQUESTED'
    }
}

const productsSuccess = result => {
    return {
        type: 'GET_PRODUCTS_SUCCESS',
        payload: result
    }
}

const productsError = error => {
    return {
        type: 'GET_PRODUCTS_ERROR',
        payload: {
            error
        }
    }
}



