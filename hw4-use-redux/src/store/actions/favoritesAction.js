import {ProductsService} from "../../services";

export const addToFavoritesAction = art => async (dispatch) => {

    if (art) {
        const products = await ProductsService.loadProducts();
        const product = products.find(item => item.articule === art);

        return dispatch ({
            type: 'ADD_TO_FAVORITES',
            payload: product
        })
    }
}

export const deleteFromFavorites = art => async dispatch => {
    return dispatch ({
        type: 'DELETE_FROM_FAVORITES',
        payload: art
    })

}