
export function modalAction(boolean) {
    return {
        type: 'IS_MODAL_ACTIVE',
        payload: boolean
    }
}