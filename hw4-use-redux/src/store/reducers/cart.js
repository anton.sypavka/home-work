import {LocalStorageService} from "../../services";

const initialState = {
    cart: LocalStorageService.getFromLocalStorage('Cart') || []
}

export function cartReducer(state = initialState, action) {

    switch (action.type) {
        case 'ADD_TO_CART' :
            return {
                ...state,
                cart: [...state.cart, action.payload]
            };

        case 'DELETE_FROM_CART' :
            return {
                ...state,
                cart: state.cart.filter( item => item.articule !== action.payload)
            };
        default: return state
    }

}