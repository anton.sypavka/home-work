import {LocalStorageService} from "../../services";

const initialState = {
    favorites: LocalStorageService.getFromLocalStorage('Favorites') || []
}

export function favoritesReducer(state = initialState, action) {

    switch (action.type) {
        case 'ADD_TO_FAVORITES' :
            return {
                ...state,
                favorites: [...state.favorites, action.payload]
            }
        case 'DELETE_FROM_FAVORITES' :
            return {
                ...state,
                favorites: state.favorites.filter( item => item.articule !== action.payload)
            }

        default: return state
    }
}