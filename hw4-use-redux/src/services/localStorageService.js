class LocalStorageService {

    static setToLocalStorage(key, products) {
        localStorage.setItem(key, JSON.stringify(products))
    }

    static getFromLocalStorage (key) {
        const store = localStorage.getItem(key)

        if (store === null) {
            return []
        }

        return JSON.parse(store)
    }

}

export default LocalStorageService;