import React from 'react';
import {useSelector} from "react-redux";
import ProductsList from "../ProductList";

function CartPage () {
    const cart = useSelector( state => state.cart.cart)

    return (
        <div>
            <ProductsList products={cart}
                          button={'closeBtn'}
            />
        </div>
    )
}

export default CartPage;