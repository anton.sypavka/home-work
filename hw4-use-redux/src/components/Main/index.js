import {Switch, Route} from 'react-router-dom';
import HomePage from '../HomePage';
import FavoritesPage from "../../components/FavoritesPage";
import CartPage from "../CartPage";

function Main(props) {

    return (
        <main>
            <Switch>
                <Route exact path='/' component={HomePage}/>
                <Route path='/favorites' component={FavoritesPage}/>
                <Route exact path='/cart' component={CartPage}/>
            </Switch>
        </main>
    )


}

export default Main;