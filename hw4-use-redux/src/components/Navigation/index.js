import {useEffect} from 'react';
import { Link } from 'react-router-dom';
import './styles.scss'
import {useSelector, useDispatch} from "react-redux";
import {addToCartAction} from "../../store/actions/cartAction";
import {addToFavoritesAction} from "../../store/actions/favoritesAction";

function Navigation() {
        const cart = useSelector( state => state.cart.cart);
        const favorites = useSelector( state => state.favorites.favorites)
        const dispatch = useDispatch();

        useEffect( () => {
                dispatch(addToCartAction())
                dispatch(addToFavoritesAction())
        }, []);

        return (
            <div className='nav-bar'>
                <Link to={`/`}>Home</Link>
                <Link to={`/favorites/`}>Favorites<i className='fas fa-star'></i>{favorites.length}</Link>
                <Link to={`/cart/`}>Cart<i className="fas fa-shopping-cart"></i>{cart.length}</Link>
            </div>
        )


}

export default Navigation;