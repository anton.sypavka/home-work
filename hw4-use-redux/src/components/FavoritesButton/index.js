import './styles.scss';

function FavoritesButton(props) {

    const onAddToFavoritesClick = () => {
        props.onClick(props.articule);
    }


    const className = props.isActive ? 'fas fa-star' : 'far fa-star';

    return (
        <span articule={props.articule} onClick={onAddToFavoritesClick} className='favorites-button'><i className={className}></i></span>
    )

}

export default FavoritesButton;
