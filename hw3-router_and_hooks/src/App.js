import React, {useEffect, useState} from 'react';
import Main from './components/Main';
import Navigation from './components/Navigation';
import {LocalStorageService, ProductsService} from "./services";
import Modal from "./components/Modal";

function App () {
    const [products, setProducts] = useState([]);
    const [favorites, setFavorites] = useState([]);
    const [cart, setCart] = useState([]);
    const [modalShow, setModalShow] = useState(false);
    const [currentProduct, setCurrentProduct] = useState(null);

    useEffect(async () => {
        const {data:products} = await ProductsService.getProducts();
        setProducts(products);
        setCart(LocalStorageService.getFromLocalStorage('CartPage'))
        setFavorites(LocalStorageService.getFromLocalStorage('Favorites'))
    }, []);

    const onAddToCartClick = ({target}) => {
        setModalShow(true);
        setCurrentProduct(target.dataset.articule);
    }

    const onModalClose = ({target}) => {
        if (target.classList.contains('modal-container') ||target.classList.contains('close-btn')) {
            setModalShow(false);
        }
    }

    const onOkBtnClick = ({target}) => {
        const art = target.closest('#modal').getAttribute('articule');
        const product = products.find( item => item.articule === art);
        const isAddedToCart = cart.find( item => item.articule === art);
        const stateCart = [...cart];

        if (isAddedToCart) {
            alert('This product has been already added to the cart');
            setModalShow(false);
        } else {
            stateCart.push(product);
            LocalStorageService.setToLocalStorage('CartPage', stateCart)
            setModalShow(false);
            setCart(stateCart);
        }
    }

    const onAddToFavoritesClick = (art) => {
        const product = products.find( item => item.articule === art);
        const favorite = favorites.find( item => item.articule === art);
        const stateFavorites = [...favorites];
        if (favorite) {
            const index = stateFavorites.findIndex(item => item.articule === art);
            if (index >= 0) {
                stateFavorites.splice(index, 1);
                LocalStorageService.setToLocalStorage('Favorites', stateFavorites);
                setFavorites(stateFavorites);
            }
        } else {
            stateFavorites.push(product);
            LocalStorageService.setToLocalStorage('Favorites', stateFavorites);
            setFavorites(stateFavorites);
        }
    }

    const onDeleteFromCart = ({target}) => {
            const art = target.closest('#modal').getAttribute('articule');
            const product = cart.find( item => item.articule === art);
            const stateCart = [...cart];

            if (product) {
                const index = stateCart.findIndex( item => item.articule === art);
                if (index >= 0) {
                    stateCart.splice(index, 1);
                    LocalStorageService.setToLocalStorage('CartPage', stateCart);
                    setModalShow(false);
                    setCart(stateCart);
                }
            }
        }

    return (
        <div className="App">
            <Navigation cart={cart} favorites={favorites}/>
            <Main products={products}
                  favorites={favorites}
                  cart={cart}
                  articule={currentProduct}
                  show={modalShow}
                  onCloseBtnClick={onModalClose}
                  onOkBtnClick={onOkBtnClick}
                  onBtnClick={onAddToCartClick}
                  onFavoritesClick={onAddToFavoritesClick}
                  onDeleteFromCart={onDeleteFromCart}
            />
        </div>
    );
}

export default App;
