import './styles.scss'

function Modal(props) {

    const onModalClose = event => {
        event.preventDefault();
        props.onCloseBtnClick(event);
    }

    const {heading, text, className, articule, actions} = props;
    const style = {display: props.show ? 'block' : 'none'};

    return (
        <div className="modal-container" style={style} onClick={onModalClose}>
            <div className={className} id={className} articule={articule}>
                <div className="modal__header">
                    <h2>{heading}</h2>
                    {props.closeButton ? <button className='close-btn' onClick={onModalClose}>X</button> : ''}
                </div>
                <div className="modal__body">
                    <p>{text}</p>
                </div>
                <div className="modal__footer">
                    {props.actions.okBtn}
                    {props.actions.closeBtn}
                </div>
            </div>
        </div>
    )

}

export default Modal;