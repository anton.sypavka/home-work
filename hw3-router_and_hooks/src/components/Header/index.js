import './styles.scss'

function Header({cart, favorites}) {

    return (
        <div className='header'>
            <ul className='header__list'>
                <li className='header__list-item'>
                    <i className="fas fa-shopping-cart"> {cart.length}</i>
                </li>
                <li className='header__list-item'>
                    <i className='fas fa-star'> {favorites.length}</i>
                </li>
            </ul>
        </div>
    );
}

export default Header;