import React, { useState, useEffect } from 'react';
import ProductList from "../ProductList";
import Modal from "../Modal";
import Button from "../../components/Button";




function HomePage ({props}) {

    return (
        <div className="container">
            <ProductList products={props.products}
                         onBtnClick={props.onBtnClick}
                         onFavoritesClick={props.onFavoritesClick}
                         favorites={props.favorites}
                         button={'addToCartBtn'}
            />
            <Modal show={props.show}
                   articule={props.articule}
                   onCloseBtnClick={props.onCloseBtnClick}
                   className='modal'
                   closeButton={true}
                   heading={'First modal'}
                   text={'Are you sure you want to add this product to your cart?'}
                   actions={
                       {
                           okBtn: (<Button style={{backgroundColor: '#007bff', color: 'white', width: '150px'}} onClick={props.onOkBtnClick} className = {'btn'} text = {'OK'}/>),
                           closeBtn: (<Button style={{backgroundColor: '#6c757d', color: 'white', width: '100px'}} onClick={props.onCloseBtnClick} className = {'btn close-btn'} text = {'Close'}/>)
                       }
                   }
           />
        </div>
    )

}

export default HomePage;