import React, { useState, useEffect } from 'react';
import ProductList from "../ProductList";
import {LocalStorageService, ProductsService} from "../../services";
import Button from "../Button";
import Modal from "../Modal";

function CartPage({props}) {
    // const [products, setProducts] = useState([]);
    // const [favorites, setFavorites] = useState([]);
    // const [cart, setCart] = useState([]);
    // const [modalShow, setModalShow] = useState(false);
    // const [currentProduct, setCurrentProduct] = useState(null);
    //
    // useEffect(async () => {
    //     const {data:products} = await ProductsService.getProducts();
    //     setProducts(products);
    //     setCart(LocalStorageService.getFromLocalStorage('CartPage'))
    //     setFavorites(LocalStorageService.getFromLocalStorage('Favorites'))
    // }, []);
    //
    // const onAddToCartClick = ({target}) => {
    //     setModalShow(true);
    //     setCurrentProduct(target.dataset.articule);
    // }
    //
    // const onModalClose = ({target}) => {
    //     if (target.classList.contains('modal-container') ||target.classList.contains('close-btn')) {
    //         setModalShow(false);
    //     }
    // }
    //
    // const onOkBtnClick = ({target}) => {
    //     const art = target.closest('#modal').getAttribute('articule');
    //     const product = cart.find( item => item.articule === art);
    //     const stateCart = [...cart];
    //
    //     if (product) {
    //         const index = stateCart.findIndex( item => item.articule === art);
    //         if (index >= 0) {
    //             stateCart.splice(index, 1);
    //             LocalStorageService.setToLocalStorage('CartPage', stateCart);
    //             setModalShow(false);
    //             setCart(stateCart);
    //         }
    //     }
    // }
    //
    // const onAddToFavoritesClick = (art) => {
    //     const product = products.find( item => item.articule === art);
    //     const favorite = favorites.find( item => item.articule === art);
    //     const stateFavorites = [...favorites];
    //     if (favorite) {
    //         const index = stateFavorites.findIndex(item => item.articule === art);
    //         if (index >= 0) {
    //             stateFavorites.splice(index, 1);
    //             LocalStorageService.setToLocalStorage('Favorites', stateFavorites);
    //             setFavorites(stateFavorites);
    //         }
    //     } else {
    //         stateFavorites.push(product);
    //         LocalStorageService.setToLocalStorage('Favorites', stateFavorites);
    //         setFavorites(stateFavorites);
    //     }
    // }

    return (
        <div className="container">
            <ProductList products={props.cart}
                         onBtnClick={props.onBtnClick}
                         onFavoritesClick={props.onFavoritesClick}
                         favorites={props.favorites}
                         button={'closeBtn'}
            />
            <Modal show={props.show}
                   articule={props.articule}
                   onCloseBtnClick={props.onCloseBtnClick}
                   className='modal'
                   closeButton={true}
                   heading={'First modal'}
                   text={'Are you sure you want to delete this product from your cart?'}
                   actions={
                       {
                           okBtn: (<Button style={{backgroundColor: '#007bff', color: 'white', width: '150px'}} onClick={props.onDeleteFromCart} className = {'btn'} text = {'OK'}/>),
                           closeBtn: (<Button style={{backgroundColor: '#6c757d', color: 'white', width: '100px'}} onClick={props.onCloseBtnClick} className = {'btn close-btn'} text = {'Close'}/>)
                       }
                   }
            />
        </div>
    )
}

export default CartPage;