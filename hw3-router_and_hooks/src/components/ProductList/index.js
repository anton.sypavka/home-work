import ProductCard from "../ProductCard";
import './styles.scss'

function ProductList(props) {

    console.log('Products from ProductsList -->', props.products)

    return (
        <div className='container'>
            <ul className='product-list'>
                {props.products.map( (product, i) => <ProductCard
                    product={product}
                    key={product.articule}
                    onBtnClick={props.onBtnClick}
                    onFavoritesClick={props.onFavoritesClick}
                    isActive={props.favorites.find(({articule}) => articule === product.articule)}
                    button={props.button}
                />)}
            </ul>
        </div>
    )
}

export default ProductList;