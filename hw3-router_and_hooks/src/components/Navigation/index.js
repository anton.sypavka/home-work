import React from 'react';
import { Link } from 'react-router-dom';
import './styles.scss'

function Navigation({favorites, cart}) {

        return (
            <div className='nav-bar'>
                <Link to={`/`}>Home</Link>
                <Link to={`/favorites/`}>Favorites<i className='fas fa-star'></i>{favorites.length}</Link>
                <Link to={`/cart/`}>Cart<i className="fas fa-shopping-cart"></i>{cart.length}</Link>
            </div>
        )


}

export default Navigation;