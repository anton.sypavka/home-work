import React from 'react';
import {Switch, Route} from 'react-router-dom';
import HomePage from '../HomePage';
import FavoritesPage from "../../components/FavoritesPage";
import CartPage from "../CartPage";


function Main(props) {

    return (
        <main>
            <Switch>
                <Route exact path='/' render={ () => <HomePage props={props}/> }/>
                <Route path='/favorites' render={ () => <FavoritesPage props={props}/> }/>
                <Route exact path='/cart' render={ () => <CartPage props={props}/> }/>
            </Switch>
        </main>
    )


}

export default Main;