import './styles.scss'
import Button from "../Button";
import FavoritesButton from "../../components/FavoritesButton";

function ProductCard(props) {

    // console.log('Products from ProductCard-->', props.product)
    const {name, articule, url, text, price, color} = props.product;

    return (
        <li key={articule} className='product-card' articule={articule}>
            <img className='product-image' src={url} alt='album cover'/>
            <h3 className='product-heading'>{name}</h3>
            <FavoritesButton
                             onClick={props.onFavoritesClick}
                             articule={articule}
                             isActive={props.isActive}
            />
            <p className='product-description'>{text}</p>
            <p className='product-price'>Price: {price}</p>
            <div className='product-color'>Color: <div className='color-item' style={{backgroundColor: color}}></div></div>
            <div className='button-container'>
                {props.button === 'addToCartBtn' ? <Button articule={articule} onClick={props.onBtnClick} className='btn add-to-cart' style={{backgroundColor: '#646b73', color: 'white', width: '100px'}} text='Add to cart'/> : ''}
                {props.button === 'closeBtn' ? <Button articule={articule} onClick={props.onBtnClick} className='btn delete-btn ' style={{backgroundColor: '#ff665b', color: 'white', width: '100px'}} text='Delete'/> : ''}
            </div>
        </li>
    )
}

export default ProductCard;