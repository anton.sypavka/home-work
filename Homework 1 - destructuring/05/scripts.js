const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

/*V1*/
const booksNew =[...books, {name, author} = bookToAdd];

/*V2*/
// const booksNew =[...books, {name: 'Game of thrones', author: 'George R. R. Martin'}];

console.log(booksNew);