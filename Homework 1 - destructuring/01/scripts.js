const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const clientsConcat = [...clients1, ...clients2];
const newClientsList = clientsConcat.sort().filter((element, pos) => clientsConcat.indexOf(element) === pos);

console.log(clientsConcat);
console.log(newClientsList);

// for (let i = 0; i < clientsConcat.length; i++) {
//     if (clientsConcat[i] !== clientsConcat[i +1]) {
//         newArr.push(clientsConcat[i]);
//     }
// }

