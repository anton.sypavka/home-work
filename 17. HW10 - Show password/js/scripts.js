function validateForm() {
    const form = document.getElementsByTagName('form').item(0);
    const input = document.getElementsByTagName('input');
    const submitButton = document.getElementsByTagName('button').item(0);


    form.addEventListener('click', function (event) {
        let target = event.target;

        if(target.tagName === 'I' || target.tagName === 'INPUT') {
            target.classList.toggle('fa-eye-slash');

            if(target.classList.contains('fa-eye-slash')){
                target.setAttribute('type', 'text');
            } else {
                target.setAttribute('type', 'password');
            }
        }
    });

    submitButton.onclick = function (event) {
        event.preventDefault()
        input[0].value === input[1].value && input[0].value && input[1].value ? showGreeting() : showError();
    };

}

validateForm()


function showGreeting() {
    const errorMessage = document.getElementById('error-message');

    if(errorMessage) {
        errorMessage.remove();
    }

    setTimeout(() => {
        alert('You are welcome!');
    }, 0);

}


function showError() {
    const errorMessage = document.getElementById('error-message');

    if(errorMessage) {
        return;
    }

    const lable = document.getElementsByClassName('input-wrapper');
    const error = document.createElement('span');
    error.setAttribute('id', 'error-message');
    error.innerHTML = 'Нужно ввести одинаковые значения';
    lable[1].after(error);
}















