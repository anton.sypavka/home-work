const root = document.getElementById('root');
const list = document.createElement('ul');
root.append(list);

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function GetBooks({author, name, price}) {
    if (!author) {
        throw new Error('Author is required!')
    }
    if (!name) {
        throw new Error('Name is required')
    }
    if (!price) {
        throw new Error('Price is required')
    }

    this.author = author;
    this.name = name;
    this.price = price;
}


for (let i = 0; i < books.length; i++) {

    try {
        const arr = [];
        arr.push(`<li>${Object.values(new GetBooks(books[i]))}</li>`);
        list.insertAdjacentHTML('afterbegin', arr.join(''));
    } catch (e) {
        console.log(e.message);
    }
}


