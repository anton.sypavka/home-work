class ProductService {
    static async loadProducts() {
        return fetch('http://localhost:3000/productsList.json')
            .then(response => response.json());
    }
}

export default ProductService;