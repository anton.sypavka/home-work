import React from 'react';
import ProductsList from "../ProductList";
import {useSelector} from "react-redux";

function FavoritesPage () {
    const favorites = useSelector( state => state.favorites.favorites);

    return (
        <div>
            <ProductsList products={favorites}
                          button={'addToCartBtn'}
            />
        </div>
    )
}

export default FavoritesPage;