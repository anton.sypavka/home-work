import ProductCard from "../ProductCard";
import './styles.scss'

function ProductList(props) {

    return (
        <div className='container'>
            <ul className='product-list'>
                {props.products.map( (product) => <ProductCard
                    product={product}
                    key={product.articule}
                    button={props.button}
                />)}
            </ul>
        </div>
    )
}

export default ProductList;