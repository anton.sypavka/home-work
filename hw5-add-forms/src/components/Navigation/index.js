import { Link } from 'react-router-dom';
import './styles.scss'
import {useSelector} from "react-redux";

function Navigation() {
        const cart = useSelector( state => state.cart.cart);
        const favorites = useSelector( state => state.favorites.favorites)

        return (
            <div className='nav-bar'>
                <Link to={`/`}>Home</Link>
                <Link to={`/favorites/`}>Favorites<i className='fas fa-star'></i>{favorites.length}</Link>
                <Link to={`/cart/`}>Cart<i className="fas fa-shopping-cart"></i>{cart.length}</Link>
            </div>
        )


}

export default Navigation;