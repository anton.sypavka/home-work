import React from 'react';
import {Formik, Form, Field} from "formik";
import * as Yup from 'yup';
import './styles.scss'

const validationSchema = Yup.object({
                firstName: Yup.string()
                    .max(15, 'Must be 15 characters or less')
                    .required('Required'),
                lastName: Yup.string()
                    .max(20, 'Must be 20 characters or less')
                    .required('Required'),
                email: Yup.string()
                    .email('Invalid email address')
                    .required('Required'),
                phoneNumber: Yup.number()
                    .integer()
                    .positive()
                    .required('Required'),
                shippingAddress: Yup.string()
                    .required('Required'),
        })

function CartForm (props) {

    const onSubmit = data => {
        props.onSubmit(data);
    }

    return (
        <Formik
            initialValues={{
                firstName: '',
                lastName: '',
                email: '',
                phoneNumber: '',
                shippingAddress: '',
            }}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
        >
            {
                ({errors, touched}) => (
                    <div className='form-container'>
                        <Form>

                            <div className='form-input'>
                                <label htmlFor='firstName'>First Name</label>
                                <Field name='firstName' type='text'/>
                                {touched.firstName && errors.firstName ? (
                                    <div style={{color: 'red', fontSize: '12px', fontFamily: 'sans-serif'}}>{errors.firstName}</div>
                                ) : null}
                            </div>

                            <div className='form-input'>
                                <label htmlFor='lastName'>Last Name</label>
                                <Field name='lastName' type='text'/>
                                {touched.lastName && errors.lastName ? (
                                    <div style={{color: 'red', fontSize: '12px', fontFamily: 'sans-serif'}}>{errors.lastName}</div>
                                ) : null}
                            </div>

                            <div className='form-input'>
                                <label htmlFor='email'>Email</label>
                                <Field name='email' type='text'/>
                                {touched.email && errors.email ? (
                                    <div style={{color: 'red', fontSize: '12px', fontFamily: 'sans-serif'}}>{errors.email}</div>
                                ) : null}
                            </div>

                            <div className='form-input'>
                                <label htmlFor='phoneNumber'>Phone number</label>
                                <Field name='phoneNumber' type='text'/>
                                {touched.phoneNumber && errors.phoneNumber ? (
                                    <div style={{color: 'red', fontSize: '12px', fontFamily: 'sans-serif'}}>{errors.phoneNumber}</div>
                                ) : null}
                            </div>

                            <div className='form-input'>
                                <label htmlFor='shippingAddress'>Shipping address</label>
                                <Field name='shippingAddress' type='text'/>
                                {touched.shippingAddress && errors.shippingAddress ? (
                                    <div style={{color: 'red', fontSize: '12px', fontFamily: 'sans-serif'}}>{errors.shippingAddress}</div>
                                ) : null}
                            </div>

                            <button type='submit'>Submit</button>
                        </Form>
                    </div>
                )
            }
        </Formik>
    )
}

export default CartForm;