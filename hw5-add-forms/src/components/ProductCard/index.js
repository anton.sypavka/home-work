import './styles.scss'
import Button from "../Button";
import FavoritesButton from "../FavoritesButton";
import {useSelector, useDispatch} from "react-redux";import {LocalStorageService} from "../../services";
import {favoritesAction} from "../../store/actions/favoritesAction";
import {useState} from "react";
import Modal from "../Modal";
import {addToCartAction, deleteFromCartAction} from "../../store/actions/cartAction";


function ProductCard(props) {
    const {name, articule, url, text, price, color} = props.product;

    const favorites = useSelector( state => state.favorites.favorites);
    const cart = useSelector( state => state.cart.cart);
    const [currentProduct, setCurrentProduct] = useState(null);
    const dispatch = useDispatch();

    const onAddToCartClick = ({target}) => {
        const articule = target.closest('.product-card').getAttribute('articule');
        setCurrentProduct(articule)
    }

    const onDeleteFromCart = ({target}) => {
        const art = target.closest('.product-card').getAttribute('articule');
        dispatch(deleteFromCartAction(art));

    }

    const onAddToFavoritesClick = (art) => {
        dispatch(favoritesAction(art));
    }

    const onOkBtnClick = ({target}) => {
        const art = target.closest('#modal').getAttribute('articule');
        const isAddedToCart = cart.find( item => item.articule === art);

        if (isAddedToCart) {
            alert('This product has been already added to the cart');
        } else {
            dispatch(addToCartAction(art));
            setCurrentProduct(null);
        }
    }



    const onModalClose = ({target}) => {
        if (target.classList.contains('modal-container') ||target.classList.contains('close-btn')) {
            setCurrentProduct(null);
        }
    }

    return (
        <li key={articule} className='product-card' articule={articule}>
            <img className='product-image' src={url} alt='album cover'/>
            <h3 className='product-heading'>{name}</h3>
            <FavoritesButton
                             onClick={onAddToFavoritesClick}
                             articule={articule}
                             isActive={favorites.find((item) => item.articule === articule)}
            />
            <p className='product-description'>{text}</p>
            <p className='product-price'>Price: {price}</p>
            <div className='product-color'>Color: <div className='color-item' style={{backgroundColor: color}}></div></div>
            <div className='button-container'>
                {props.button === 'addToCartBtn' ? <Button articule={articule} onClick={onAddToCartClick} className='btn add-to-cart' style={{backgroundColor: '#646b73', color: 'white', width: '100px'}} text='Add to cart'/> : ''}
                {props.button === 'closeBtn' ? <Button articule={articule} onClick={onDeleteFromCart} className='btn delete-btn ' style={{backgroundColor: '#ff665b', color: 'white', width: '100px'}} text='Delete'/> : ''}
            </div>
            {currentProduct
                ? <Modal closeButton={true}
                                     onCloseBtnClick={onModalClose}
                                     articule={currentProduct}
                                     className='modal'
                                     heading={'First modal'}
                                     text={'Are you sure you want to add this product to your cart?'}
                                     actions={
                                         {
                                             okBtn: (<Button style={{backgroundColor: '#007bff', color: 'white', width: '150px'}} onClick={onOkBtnClick} className = {'btn'} text = {'OK'}/>),
                                             closeBtn: (<Button style={{backgroundColor: '#6c757d', color: 'white', width: '100px'}} onClick={onModalClose}  className = {'btn close-btn'} text = {'Close'}/>)
                                         }
                                     }
                  />
                : null}
        </li>
    )
}

export default ProductCard;