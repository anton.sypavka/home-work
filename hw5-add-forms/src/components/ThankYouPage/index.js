import React from 'react';
import './styles.scss';

function ThankYouPage () {
    return (
        <div className='container'>
            <div className='heading'>
                <i className="far fa-check-circle"></i>
                <h1>Thank you for your order!</h1>
            </div>
        </div>
    )
}

export default ThankYouPage;