import React from 'react';
import {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {loadProducts} from "../../store/actions/productsAction";
import ProductsList from "../ProductList";

function HomePage () {
    const products = useSelector( state => state.products.products);
    const dispatch = useDispatch();

    useEffect( () => {
        dispatch(loadProducts())
    }, [])

    return (
        <div>
            <ProductsList products={products}
                          button={'addToCartBtn'}
            />
        </div>
    )
}

export default HomePage;