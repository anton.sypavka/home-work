import './styles.scss'

function Modal(props) {

    const {heading, text, className, articule} = props;

    const onModalClose = event => {
        event.preventDefault();
        props.onCloseBtnClick(event);
    }

    return (
        <div className="modal-container" onClick={onModalClose}>
            <div className={className} id={className} articule={articule}>
                <div className="modal__header">
                    <h2>{heading}</h2>
                    {props.closeButton ? <button className='close-btn' >X</button> : ''}
                </div>
                <div className="modal__body">
                    <p>{text}</p>
                </div>
                <div className="modal__footer">
                    {props.actions.okBtn}
                    {props.actions.closeBtn}
                </div>
            </div>
        </div>
    )

}

export default Modal;