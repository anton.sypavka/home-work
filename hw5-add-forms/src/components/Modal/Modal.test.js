import React from "react";
import { shallow } from 'enzyme';
import Modal from "./index";
import Button from "../Button";

describe ('Modal', () => {
    const onBtnClick = jest.fn();
    const props = {
        heading: 'Modal heading',
        text: 'Modal text',
        className: 'modal',
        actions: {
            okBtn: (<Button style={{backgroundColor: '#007bff', color: 'white', width: '150px'}} onClick={onBtnClick} className = {'btn'} text = {'OK'}/>),
        },
        articule: 123,
        closeBtn: true
    }

    describe ('renders with props', () => {
        const container = shallow(<Modal {...props}/>);

        it ('renders properly', () => {
            expect(container).toMatchSnapshot();
        })

        it ('renders with text', () => {
            expect(container.find('p').text()).toEqual('Modal text');
        })
    })

    describe ('calls onClick function when button clicked', () => {
        const container = shallow(<Modal {...props} onClick={onBtnClick}/>);

        it ('calls onClick function', () => {
            container.find('Button').simulate('click');
            expect(onBtnClick.mock.calls.length).toEqual(1)
        })
    })

});