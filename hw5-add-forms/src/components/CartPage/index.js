import {React, useState} from 'react';
import {Redirect} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import ProductsList from "../ProductList";
import CartForm from "../CartForm"
import {orderSuccess} from "../../store/actions/cartAction";


function CartPage () {
    const cart = useSelector( state => state.cart.cart)
    const dispatch = useDispatch();
    const [redirect, setRedirect] = useState(false);

    const onSubmit = data => {

        const orderDetails = {
                cart: cart,
                ...data
            }

        dispatch(orderSuccess([]));
        setRedirect(true);

        console.log('Order details-->', orderDetails);
    }

    return (
        <div className='container'>
            <ProductsList products={cart}
                          button={'closeBtn'}
            />
            <CartForm onSubmit={onSubmit}/>
            { redirect ? <Redirect to='/thankyoupage'/> : null }
        </div>
    )
}

export default CartPage;