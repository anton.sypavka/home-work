import React from "react";
import { shallow } from 'enzyme';
import Button from "./index";

describe ('button', () => {
    const onBtnClick = jest.fn();
    const props = {
        text: 'Add to cart',
        articule: 123,
        className: 'add-to-cart',
    }

    describe ( 'renders button text content', () => {
        const container = shallow(<Button {...props}/>);

        it('renders button text content', () => {
            expect(container.find('button').text()).toEqual('Add to cart');
        })

        it('renders properly', () => {
            expect(container).toMatchSnapshot();
        })
    })

    describe ( 'calls onClick function when button clicked', () => {
        const container = shallow(<Button {...props} onClick={onBtnClick}/>);

        it('cals onClick function', () => {
            container.find('button').simulate('click');
            expect(onBtnClick.mock.calls.length).toEqual(1);
        })
    })
})


// let container = null;
//
// beforeEach ( () => {
//     container = document.createElement('div');
//     document.body.appendChild(container);
// });
//
// afterEach ( () => {
//     unmountComponentAtNode(container);
//     container.remove();
//     container = null;
// });
//
// it ('renders button', () => {
//     const props = {
//         text: 'Add to cart',
//         articule: 123,
//         className: 'add-to-cart',
//     }
//
//     act( () => {
//         render(<Button {...props} />, container);
//     });
//     expect(container.textContent).toBe('Add to cart');
//     expect(container.querySelector('button').getAttribute('data-articule')).toEqual('123');
//     expect(container.querySelector('button').getAttribute('class')).toEqual('add-to-cart');
//
// });
//
// it ('calls onClick function when button clicked', () => {
//     const onClick = jest.fn();
//
//     act( () => {
//         render(<Button onClick={onClick}/>, container);
//     })
//
//     const button = document.querySelector('button');
//
//     act( () => {
//         button.dispatchEvent(new MouseEvent('click', { bubbles: true }));
//     })
//     expect(onClick).toHaveBeenCalledTimes(1);
//
//     act(() => {
//         for (let i = 0; i < 5; i++) {
//             button.dispatchEvent(new MouseEvent("click", { bubbles: true }));
//         }
//     });
//     expect(onClick).toHaveBeenCalledTimes(6);
// });

