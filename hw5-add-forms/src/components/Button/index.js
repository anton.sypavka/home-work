import './styles.scss';

function Button({text, style, articule, className, onClick}) {

    const onBtnClick = (event) => {
        onClick(event);
    }

    return (
        <button data-articule={articule} onClick={onBtnClick} className={className} style={style}>{text}</button>
    )

}

export default Button;