import {Switch, Route} from 'react-router-dom';
import HomePage from '../HomePage';
import FavoritesPage from "../../components/FavoritesPage";
import CartPage from "../CartPage";
import ThankYouPage from "../ThankYouPage";

function Main(props) {

    return (
        <main>
            <Switch>
                <Route exact path='/' component={HomePage}/>
                <Route path='/favorites' component={FavoritesPage}/>
                <Route exact path='/cart' component={CartPage}/>
                <Route exact path='/thankyoupage' component={ThankYouPage}/>
            </Switch>
        </main>
    )


}

export default Main;