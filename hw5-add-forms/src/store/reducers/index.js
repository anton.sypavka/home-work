import {combineReducers} from 'redux';
import {productsReducer} from "./products";
import {modalReducer} from "./modal";
import {cartReducer} from "./cart";
import {favoritesReducer} from "./favorites";

export const rootReducer = combineReducers({
    products: productsReducer,
    modal: modalReducer,
    cart: cartReducer,
    favorites: favoritesReducer
})