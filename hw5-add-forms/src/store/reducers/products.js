
const initialState = {
    loading: false,
    products: [],
    error: null
}

export function productsReducer(state = initialState, action) {
    switch (action.type) {
        case 'GET_PRODUCTS_REQUESTED' :
            return {
                ...state,
                loading:true
            };
        case 'GET_PRODUCTS_SUCCESS' :
            return {
                ...state,
                loading: false,
                error: null,
                products: action.payload
            };
        case 'GET_PRODUCTS_ERROR' :
            return {
                ...state,
                loading: false,
                error: action.payload.error
            }
        default: return state;
    }

    // switch(action.type) {
    //     case 'LOAD_PRODUCTS':
    //         return {
    //             ...state,
    //             products: action.payload
    //         }
    //     default: return state
    // }

}