
const initialState = {
    isModalActive: false
}

export function modalReducer (state = initialState, action) {
    switch (action.type) {
        case 'IS_MODAL_ACTIVE':
            return {
                ...state,
                isModalActive: action.payload
            };
        default:
            return state;
    }
}