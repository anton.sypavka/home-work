import {createStore, applyMiddleware} from 'redux';
import {rootReducer} from "./reducers";
import thunk from "redux-thunk";
import {LocalStorageService} from "../services";

const saveState = (state) => {
    const {cart: {cart}, favorites: {favorites}} = state;
    LocalStorageService.setToLocalStorage('Cart', cart)
    LocalStorageService.setToLocalStorage('Favorites', favorites)

}

export const store = createStore(rootReducer, applyMiddleware(thunk));

store.subscribe( () => {
    saveState(store.getState());
})

