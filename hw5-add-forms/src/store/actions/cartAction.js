import {ProductsService} from "../../services";

export const addToCartAction = art => async (dispatch) => {

        if (art) {
            const products = await ProductsService.loadProducts();
            const product = products.find(item => item.articule === art);

            return dispatch ({
                type: 'ADD_TO_CART',
                payload: product
            })
        }
}

export const deleteFromCartAction = art => {

    return {
        type: 'DELETE_FROM_CART',
        payload: art
    }
}


export const orderSuccess = arr => {

    return {
        type: 'ORDER_SUCCESS',
        payload: arr
    }
}