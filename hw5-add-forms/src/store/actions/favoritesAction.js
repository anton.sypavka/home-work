import {ProductsService} from "../../services";


export const favoritesAction = art => async (dispatch, getState) => {
    const products = await ProductsService.loadProducts();
    const product = products.find(item => item.articule === art);
    const {favorites: {favorites}} = getState();
    const favorite = favorites.find( item => item.articule === art);

    if (favorite) {
        dispatch(deleteFromFavorites(art));
    } else {
        dispatch(addToFavorites(product));
    }
}

export const addToFavorites = articule => {

        return{
            type: 'ADD_TO_FAVORITES',
            payload: articule
        }
}

export const deleteFromFavorites = favorite =>  {
    return {
        type: 'DELETE_FROM_FAVORITES',
        payload: favorite
    }

}