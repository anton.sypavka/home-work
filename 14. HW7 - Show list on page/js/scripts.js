
function buildList(arr) {
    const newArray = '<ul>' + (arr.map(function (element) {
        return `<li>${element}</li>`
    }).join('')) + '</ul>';

    document.write(newArray);
}


const array1 = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const newArray1 = buildList(array1);
console.log(newArray1);
