/*Пустой объект с полями name и lastName*/

const student = {
    name: 'Name',
    lastName: 'Last Name',
    tabel: {}
}
const studentName = prompt('What is your name?');
const studentLastName = prompt('What is your surname?');

/*Спрашиваем и записываем полученные имя и фамилию в соответствующие поля объекта*/
student[name] = studentName;
student['lastName'] = studentLastName;


/*Спрашиваем и записываем названия предметов и баллы, если пользователь нажимает отмена цикл прекращается*/
let property;
let value;
do {
    property = prompt('Введите предмет');
    value = prompt('Введите оценку');

    if (property === null || value === null){
        break;
    } else {
        student.tabel[property] = +value;
    }

} while (property || value)
console.log(student);


/*Перебор свойств объекта: поиск и суммирование кол-ва оценок включая оценок ниже 4*/
let sum = 0;
let counter = 0;
let marksQuantity = 0;
for (key in student.tabel) {
    if (student.tabel[key] < 4) {
        counter++;
    }
    sum +=student.tabel[key];
    marksQuantity++;
}

let average = sum / marksQuantity;

if (average > 7) {
    console.log('Студенту назначена стипендия');
} else if (average >= 4 && counter === 0){
    console.log('Студент переведен на следующий курс');
} else  {
    console.log('Кол-во оценок ниже 4 -> ' +counter);
}

// console.log('Сумма балов -> ' +sum);
// console.log('Среднее арифметическое -> ' +average);
// console.log('Кол-во оценок ниже 4 -> ' +counter);



