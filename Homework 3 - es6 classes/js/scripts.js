class Employee {
    constructor(name = 'Anton', age = 29, salary = 50000) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(value) {
        if (value.length < 4) {
            throw new Error('Name is too short');
            return;
        }
        this._name = value;
    }

    set age(value) {
        this._age = value;
    }

    set salary(value) {
        this._salary = value;
    }
};

class Programmer extends Employee{
    constructor(name, age, salary) {
        super(name, age, salary);
        this.lang = ['russian', 'ukr'];
    }

    get employeeSalary () {
        return this.salary * 3;
    }
}

const copy1 = new Employee('Max', 15, 25000);
const copy2 = new Programmer('Petro', 18, 35000);
const copy3 = new Programmer('Vasyl', 28, 5000);

console.log(copy1);
console.log(copy2);