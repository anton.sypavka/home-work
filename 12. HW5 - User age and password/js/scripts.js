function createNewUser() {
    const name = prompt('Введите ваше имя');
    const lastName = prompt('Введите вашу фамилию');
    const date = (prompt('Введите вашу дату рождения в формате dd.mm.yyyy', '05.15.1991'));

    return newUser = {
        firstName: name,
        lastName: lastName,
        birthday: new Date (date.slice(6,10) + '-' + date.slice(0,2) + '-' + date.slice(3,5)),
        getAge: function () {
            const userAge = Math.floor((Date.now() - this.birthday.getTime()) / 1000 / 3600 / 24 / 365);
            return userAge;
        },
        getLogin: function () {
            return this.firstName.toLocaleLowerCase()[0] + this.lastName.toLowerCase();
            },
        getPassword: function () {
            return this.firstName.toLocaleUpperCase()[0] + this.lastName.toLowerCase() + this.birthday.toDateString().slice(11, 16);

        }
    }
}

createNewUser();

console.log(newUser);
console.log('Your Log-in: ' + newUser.getLogin());
console.log('Your Age: ' + newUser.getAge());
console.log('Your Password: ' + newUser.getPassword());

alert(`Your Log-in: ${newUser.getLogin()}

Your Password: ${newUser.getPassword()}

Your age: ${newUser.getAge()}`);











