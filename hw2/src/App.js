import React, {Component} from 'react';
import {ProductsService, LocalStorageService} from './services';
import ProductsList from './components/ProductsList';
import Modal from "./components/Modal";
import Button from "./components/Button";
import Header from "./components/Header";


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
      show: false,
      cart: [],
      favorites: [],
      currentProduct: null,
    }

    this.onOkBtnClick = this.onOkBtnClick.bind(this);
    this.onModalClose = this.onModalClose.bind(this);
    this.onAddToCartClick = this.onAddToCartClick.bind(this);
    this.onAddToFavoritesClick = this.onAddToFavoritesClick.bind(this);
  }

  async componentDidMount() {
    const {data:products} = await ProductsService.getProducts();
      this.setState((state) => ({
          products: products,
          cart: LocalStorageService.getFromLocalStorage('CartPage'),
          favorites: LocalStorageService.getFromLocalStorage('Favorites')
      }))
  }

    onOkBtnClick(event) {
        const art = event.target.closest('#modal').getAttribute('articule');
        const product = this.state.products.find( item => item.articule === art);
        const cart = [...this.state.cart];

        LocalStorageService.setToLocalStorage('CartPage', this.state.cart)

        if (product) {
            cart.push(product)
            LocalStorageService.setToLocalStorage('CartPage', cart);
            this.setState((state) => ({
                show: false,
                cart: cart
            }))
        }
    }

    onModalClose(event) {
        const {target} = event;
        console.log(target);
        if (target.classList.contains('modal-container') || target.classList.contains('close-btn')) {
            this.setState({
                show: false
            })
        }
    }

    onAddToCartClick(event) {
        this.setState({
            show: true,
            currentProduct: event.target.dataset.articule
        })

        console.log(this.state.currentProduct)
    }

    onAddToFavoritesClick(art) {
      const product = this.state.products.find( item => item.articule === art);
      const favorite = this.state.favorites.find( item => item.articule === art);
      const favorites = [...this.state.favorites];
      if (favorite) {
          const index = favorites.findIndex(item => item.articule === art);
          if (index >= 0) {
              favorites.splice(index, 1);
              LocalStorageService.setToLocalStorage('Favorites', favorites);
              this.setState({favorites})
          }
      } else {
          favorites.push(product);
          LocalStorageService.setToLocalStorage('Favorites', favorites);
          this.setState({favorites})
      }
    }


  render() {

      return (
        <div className="App">
          <Header cart={this.state.cart}
                  favorites={this.state.favorites}
          />
          <ProductsList onClick={this.onAddToCartClick}
                        products={this.state.products}
                        onFavoritesClick={this.onAddToFavoritesClick}
                        favorites={this.state.favorites}
          />
          <Modal show={this.state.show}
                 articule={this.state.currentProduct}
                 onCloseBtnClick={this.onModalClose}
                 className='modal'
                 closeButton={true}
                 heading={'First modal'}
                 text={'Are you sure you want to add this product to your cart?'}
                 actions={
                     {
                         okBtn: (<Button style={{backgroundColor: '#007bff', color: 'white', width: '150px'}} onClick={this.onOkBtnClick} className = {'btn'} text = {'OK'}/>),
                         closeBtn: (<Button style={{backgroundColor: '#6c757d', color: 'white', width: '100px'}} onClick={this.onModalClose} className = {'btn close-btn'} text = {'Close'}/>)
                     }
                 }/>
        </div>
    );
  }

}

export default App;
