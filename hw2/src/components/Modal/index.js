import React, {Component} from 'react';
import './styles.scss'

class Modal extends Component {
    constructor(props) {
        super(props);

        this.onModalClose = this.onModalClose.bind(this);
    }




    onModalClose(event) {
        event.preventDefault();
        this.props.onCloseBtnClick(event)
    }

    render() {
        const {heading, text, className, articule, actions} = this.props;
        const style = {display: this.props.show ? 'block' : 'none'};


        return (
            <div onClick={this.onModalClose} className="modal-container" style={style}>
                <div className={className} id={className} articule={articule}>
                    <div className="modal__header">
                        <h2>{heading}</h2>
                        {this.props.closeButton ? <button className='close-btn' onClick={this.onModalClose}>X</button> : ''}
                    </div>
                    <div className="modal__body">
                        <p>{text}</p>
                    </div>
                    <div className="modal__footer">
                        {actions.okBtn}
                        {actions.closeBtn}
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal;