import React, {Component} from 'react';
import PropTypes from 'prop-types'
import './styles.scss';

class FavoritesButton extends Component {
    constructor(props) {
        super(props);

        this.onAddToFavoritesClick = this.onAddToFavoritesClick.bind(this);
    }

    onAddToFavoritesClick(articule) {
        this.props.onClick(articule)
    }




    render() {
        const className = this.props.isActive ? 'fas fa-star' : 'far fa-star';

        return (
            <span articule={this.props.articule} onClick={this.onAddToFavoritesClick} className='favorites-button'><i className={className}></i></span>
        )
    }
}

export default FavoritesButton;
