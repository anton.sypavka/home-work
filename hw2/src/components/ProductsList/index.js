import React, {Component} from 'react';
import ProductCard from "../ProductCard";
import './styles.scss';
import {LocalStorageService} from '../../services';

class ProductsList extends Component {
    constructor(props) {
        super(props);

        this.onAddToCartClick = this.onAddToCartClick.bind(this);
        this.onAddToFavoritesClick = this.onAddToFavoritesClick.bind(this);
    }

    onAddToCartClick(event) {
        this.props.onClick(event);
    }

    onAddToFavoritesClick(articule) {
        this.props.onFavoritesClick(articule);
    }

    render() {

        return (
            <div className='container'>
                <ul className='product-list'>
                    {this.props.products.map( (product, i) => <ProductCard
                        onClick={this.onAddToCartClick}
                        onFavoritesClick={this.onAddToFavoritesClick}
                        articule={product.articule}
                        isActive={this.props.favorites.find(({articule}) => articule === product.articule)}
                        key={product.articule} product={product}
                    />)}
                </ul>
            </div>
        )
    }

}

export default ProductsList;