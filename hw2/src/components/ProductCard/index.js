import React, {Component} from 'react';
import Button from "../Button";
import './styles.scss'
import PropTypes from 'prop-types'
import FavoritesButton from "../FavoritesButton";


class ProductCard extends Component {
    constructor(props) {
        super(props);

        this.onAddToCartClick = this.onAddToCartClick.bind(this);
        this.onAddToFavoritesClick = this.onAddToFavoritesClick.bind(this);
    }

    onAddToCartClick(event) {
        this.props.onClick(event);
    }

    onAddToFavoritesClick() {
        this.props.onFavoritesClick(this.props.articule);

    }

    render() {
        const {name, url, price, articule, color, text} = this.props.product;


        return (
            <li className='product-card' articule={articule}>
                <img className='product-image' src={url} alt='album cover'/>
                <h3 className='product-heading'>{name}</h3>
                <FavoritesButton onClick={this.onAddToFavoritesClick}
                                 articule={articule}
                                isActive={this.props.isActive}
                />
                <p className='product-description'>{text}</p>
                <div className='product-color'>Color: <div className='color-item' style={{backgroundColor: color}}></div></div>
                <p className='product-price'>Price: {price}</p>
                <Button articule={articule} onClick={this.onAddToCartClick} className='btn add-to-cart' style={{backgroundColor: '#646b73', color: 'white', width: '100px'}} text='Add to cart'/>
            </li>
        )
    }
}

ProductCard.propTypes = {
    name: PropTypes.string,
    url: PropTypes.string,
    color: PropTypes.string,
    text: PropTypes.string,

}

export default ProductCard;

