import React, {Component, PureComponent} from 'react';
import {LocalStorageService} from '../../services'
import './styles.scss'

class Header extends PureComponent {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className='header'>
                <ul className='header__list'>
                    <li className='header__list-item'>
                        <i className="fas fa-shopping-cart"> {this.props.cart.length}</i>
                    </li>
                    <li className='header__list-item'>
                        <i className='fas fa-star'> {this.props.favorites.length}</i>
                    </li>
                </ul>
            </div>
        );
    }

}

export default Header;