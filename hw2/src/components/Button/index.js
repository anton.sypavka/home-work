import React, {Component} from 'react';
import './styles.scss';

class Button extends Component {
    constructor(props) {
        super(props);
        this.onBtnClick = this.onBtnClick.bind(this);
    }

    onBtnClick (event) {
        this.props.onClick(event);
    }


    render() {
        const {text, style, articule, className} = this.props;

        return (
            <button data-articule={articule} onClick={this.onBtnClick} className={className} style={style}>{text}</button>
        )
    }
}

export default Button;