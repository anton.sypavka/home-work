import Axios from 'axios';


class ProductsService {

    static getProducts () {
        return Axios.get('http://localhost:3000/productsList.json');
    }

}

export default ProductsService;
