import React, {Component} from 'react';
import Button from './components/Button'
import Modal from "./components/Modal";


class App extends Component {
    constructor(props) {
        super(props);
        this.onFirstBtnClick = this.onFirstBtnClick.bind(this);
        this.onSecondBtnClick = this.onSecondBtnClick.bind(this);
        this.onEmptyZoneClick = this.onEmptyZoneClick.bind(this);
        this.onOkBtnClick = this.onOkBtnClick.bind(this);
        this.onCloseBtnClick = this.onCloseBtnClick.bind(this);

        this.state = {
            firstModalShow: false,
            secondModalShow: false
        }
    }

    onFirstBtnClick() {
        this.setState({
            firstModalShow: true,
            secondModalShow: false
        })
    }

    onSecondBtnClick() {
        this.setState({
            secondModalShow: true,
            firstModalShow: false
        })
    }

    onEmptyZoneClick(event) {
        const target = event.target;

        if (target.className === 'modal-container') {
            this.setState({
                firstModalShow: false,
                secondModalShow: false
            })
        }
    }

    onOkBtnClick() {
        alert('Your operation has been completed');
        this.setState({
            firstModalShow: false,
            secondModalShow: false
        })
    }

    onCloseBtnClick() {
        this.setState({
            firstModalShow: false,
            secondModalShow: false
        })
    }


    render() {
        const firstModal = this.state.firstModalShow;

        return (
            <div className="App" style={{margin: '0 auto', width: '50%', textAlign: 'center'}}>
                <Button style={{backgroundColor: '#0069d9', color: 'white', width: '200px', marginRight: '10px'}} onClick={this.onFirstBtnClick} className = {'btn'} text = {'Open first modal'}/>
                <Button style={{backgroundColor: '#218838', color: 'white', width: '200px'}} onClick={this.onSecondBtnClick} className = {'btn'} text = {'Open second modal'}/>
                {firstModal
                    ? <Modal onEmptyZoneClick={this.onEmptyZoneClick} onClick={this.onCloseBtnClick} show={this.state.firstModalShow} closeButton={true} className={'modal first-modal'} heading={'First modal'} text={'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'}
                    actions={
                        {
                        okBtn: (<Button style={{backgroundColor: '#cf2820', color: 'white', width: '100px'}} onClick={this.onOkBtnClick} className = {'btn'} text = {'OK'}/>),
                        closeBtn: (<Button style={{backgroundColor: '#cf2820', color: 'white', width: '100px'}} onClick={this.onCloseBtnClick} className = {'btn'} text = {'Close'}/>)
                        }
                    }/>
                    : <Modal onEmptyZoneClick={this.onEmptyZoneClick} onClick={this.onCloseBtnClick} show={this.state.secondModalShow} closeButton={true} className={'modal second-modal'} heading={'Second modal'} text={'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'}
                    actions={
                        {
                        okBtn: (<Button style={{backgroundColor: '#09a849', color: 'white', width: '100px'}} onClick={this.onOkBtnClick} className = {'btn'} text = {'OK'}/>),
                        closeBtn: (<Button style={{backgroundColor: '#09a849', color: 'white', width: '100px'}} onClick={this.onCloseBtnClick} className = {'btn'} text = {'Close'}/>)
                        }
                    }/>
                }
            </div>
        )
    }

}

export default App;
