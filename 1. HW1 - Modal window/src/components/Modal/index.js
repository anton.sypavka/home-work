import React, {Component} from 'react';
import './styles.scss'

class Modal extends Component {
    constructor(props) {
        super(props);
        this.onCloseBtnClick = this.onCloseBtnClick.bind(this);
        this.onDialogClose = this.onDialogClose.bind(this);
    }


    onCloseBtnClick() {
        this.props.onClick()
    }

    onDialogClose(event) {
        event.preventDefault();
        this.props.onEmptyZoneClick(event)
    }

    render() {
        const {heading, text, className, actions} = this.props;
        const style = {display: this.props.show ? 'block' : 'none'};

        return (
            <div onClick={this.onDialogClose} className="modal-container" style={style}>
                <div className={className}>
                    <div className="modal__header">
                        <h2>{heading}</h2>
                        {this.props.closeButton ? <button onClick={this.onCloseBtnClick}>X</button> : ''}
                    </div>
                    <div className="modal__body">
                        <p>{text}</p>
                    </div>
                    <div className="modal__footer">
                        {actions.okBtn}
                        {actions.closeBtn}
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal;