import React, {Component} from 'react';
import './styles.scss';

class Button extends Component {
    constructor(props) {
        super(props);
        this.onBtnClick = this.onBtnClick.bind(this);
    }

    onBtnClick () {
        this.props.onClick();
    }


    render() {
        const {text, style, className} = this.props;

        return (
            <button onClick={this.onBtnClick} className={className} style={style}>{text}</button>
        )
    }
}

export default Button;