document.addEventListener('DOMContentLoaded', onReady);

/*
** Первый вариант с добавлением классов.

function onReady() {

    const container = document.getElementById('container');
    const changeThemeButton = document.getElementById('change-theme');
    const submitButton = document.getElementById('submit');
    const facebookButton = document.getElementById('facebook');
    const googleButton = document.getElementById('google');

    if(localStorage.getItem('Theme') === 'custom-theme') {

        container.classList.add('custom-theme-container');
        submitButton.classList.add('custom-theme-submit');
        facebookButton.classList.add('custom-theme-facebook');
        googleButton.classList.add('custom-theme-google');

    }

    changeThemeButton.addEventListener('click', function () {

        if(localStorage.getItem('Theme') === 'custom-theme'){
            container.classList.remove('custom-theme-container');
            submitButton.classList.remove('custom-theme-submit');
            facebookButton.classList.remove('custom-theme-facebook');
            googleButton.classList.remove('custom-theme-google');
            localStorage.setItem('Theme', 'default-theme');

        } else {
            container.classList.add('custom-theme-container');
            submitButton.classList.add('custom-theme-submit');
            facebookButton.classList.add('custom-theme-facebook');
            googleButton.classList.add('custom-theme-google');
            localStorage.setItem('Theme', 'custom-theme');
        }

    });
}*/

/*
** Второй вариант с добавлением/удалением файла со стилями.
 */

function onReady() {

    const head = document.getElementsByTagName('head');
    const customStyle = document.createElement('link');
    customStyle.setAttribute('rel', 'stylesheet');
    customStyle.setAttribute('href', 'css/custom-styles.min.css');
    const changeThemeButton = document.getElementById('change-theme');

    if(localStorage.getItem('Theme') === 'custom-theme') {
        head[0].append(customStyle);
    }

    changeThemeButton.addEventListener('click', function () {

        if(localStorage.getItem('Theme') === 'custom-theme') {
            customStyle.remove();
            localStorage.setItem('Theme', 'default-theme');
        } else {
            localStorage.setItem('Theme', 'custom-theme');
            head[0].append(customStyle);
        }
    })
};














