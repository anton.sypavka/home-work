const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const clean = require('gulp-clean');
const autoprefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify-es').default;
// const htmlmin = require('gulp-htmlmin');
const imagemin = require('gulp-imagemin');
// const minify = require('gulp-minify');
const browserSync = require('browser-sync').create();

sass.compiler = require('node-sass');

gulp.task('clean-dist', function () {
    return gulp.src('dist/')
        .pipe(clean({force: true}))
        .pipe(gulp.dest('dist'));
});

gulp.task('style:build', function () {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({cascade: false}))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());
});

gulp.task('style:vendor', function () {
    return gulp
        .src([
            './node_modules/@fortawesome/fontawesome-free/css/all.css',
            './node_modules/reset-css/reset.css'
        ])
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());
});

gulp.task('html:copy', () => {
    return gulp.src('src/index.html')
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.stream());
});

gulp.task('script:build', function () {
    return gulp
        .src('src/js/*.js')
        .pipe(uglify())
        .pipe(concat('script.min.js'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('script:vendor', function () {
    return gulp
        .src([
            './node_modules/jquery/dist/jquery.js',
            './node_modules/@fortawesome/fontawesome-free/js/all.js',
        ])
        .pipe(uglify())
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('dist/js'))
});

gulp.task('img:build', function () {
    return gulp
        .src('src/img/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'));
});

gulp.task('build',
    gulp.series([
            'clean-dist',
            'style:build',
            'style:vendor',
            'html:copy',
            'script:build',
            'script:vendor',
            'img:build'
        ]
    )
);

gulp.task('dev', () => {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });

    gulp.watch("src/**/*.*", gulp.series('build')).on('change',browserSync.reload);
});
